require 'rails_helper'

RSpec.feature "Categories" do
  given(:taxonomy) { create(:taxonomy) }
  given(:taxon) { taxonomy.root }
  given(:image) { create(:image) }
  given(:variant) { create(:variant, images: [image]) }
  given!(:product) { create(:product, taxons: [taxon], variants: [variant]) }
  given!(:bag) { taxonomy.root.children.create(name: "bag") }
  given!(:mug) { taxonomy.root.children.create(name: "mug") }
  given(:bag_image) { create(:image) }
  given(:mug_image) { create(:image) }
  given!(:variant_bag) { create(:variant, images: [bag_image]) }
  given!(:variant_mug) { create(:variant, images: [mug_image]) }
  given!(:test_bag) { create(:product, taxons: [bag], variants: [variant_bag], name: "test_bag") }
  given!(:test_mug) { create(:product, taxons: [mug], variants: [variant_mug], name: "test_mug") }

  background do
    visit potepan_category_path(taxon.id)
  end

  scenario "ページのコンテンツを表示する" do
    expect(page).to have_content "商品カテゴリー"
    expect(page).to have_content product.price
    within '.side-nav' do
      expect(page).to have_content taxon.name
      expect(page).to have_content taxonomy.name
      expect(page).to have_content bag.name
    end
  end

  scenario "category_panelからカテゴリーページに移動する" do
    within '.side-nav' do
      click_link taxon.name
      expect(current_path).to eq potepan_category_path(taxon.id)
      expect(page).to have_content taxon.name
    end
  end

  scenario "category_panelからbagカテゴリーページに移動する" do
    within '.side-nav' do
      click_link bag.name
      expect(current_path).to eq potepan_category_path(bag.id)
      expect(page).to have_title "#{bag.name} - BIGBAG Store"
    end
  end

  scenario "bagカテゴリーページにmugカテゴリーの商品が表示されない" do
    visit potepan_category_path(bag.id)
    expect(page).to have_content test_bag.name
    expect(page).not_to have_content test_mug.name
  end

  scenario "表示された商品名をクリックすると、商品詳細画面に移動する" do
    expect(page).to have_content product.name
    click_link product.name
    expect(current_path).to eq potepan_product_path(product.id)
    expect(page).to have_content product.description
  end
end
