require 'rails_helper'

RSpec.feature "Products" do
  given(:taxon) { create :taxon }
  given(:image) { create(:image) }
  given(:variant) { create(:variant, images: [image]) }
  given!(:product) { create(:product, taxons: [taxon], variants: [variant]) }
  given!(:product_without_taxon) { create(:product, variants: [variant]) }

  background do
    visit potepan_product_path(product.id)
  end

  scenario "ページのコンテンツが表示される" do
    expect(page).to have_content product.name
    expect(page).to have_content product.description
    expect(page).to have_content "カートへ入れる"
    expect(page).to have_content "一覧ページへ戻る"
  end

  scenario "一覧ページへ戻るリンクで一覧ページに戻れる" do
    expect(page).not_to have_content "商品カテゴリー"
    expect(page).to have_content "一覧ページへ戻る"
    click_link "一覧ページへ戻る"
    expect(page).to have_content "商品カテゴリー"
  end

  scenario "taxonが紐づいていない商品の詳細ページには、一覧ページへ戻るリンクが表示されない" do
    visit potepan_product_path(product_without_taxon.id)
    expect(page).not_to have_content "一覧ページへ戻る"
  end
end
