require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "full_title_method" do
    context "no argument" do
      it "BIGBAG Store というタイトルを返す" do
        expect(helper.full_title).to eq("BIGBAG Store")
      end
    end

    context "page_title is empty" do
      it "BIGBAG Store というタイトルを返す" do
        expect(helper.full_title(page_title: "")).to eq("BIGBAG Store")
      end
    end

    context "page_title is nil" do
      it "BIGBAG Store というタイトルを返す" do
        expect(helper.full_title(page_title: nil)).to eq("BIGBAG Store")
      end
    end

    context "page_title is not empty" do
      it "Product name - BIGBAG Store というタイトルを返す" do
        expect(helper.full_title(page_title: "Product Name")).to eq("Product Name - BIGBAG Store")
      end
    end
  end
end
