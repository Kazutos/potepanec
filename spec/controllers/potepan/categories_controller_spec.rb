require 'rails_helper'
RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "GET #show" do
    let(:test_taxonomy) { create :taxonomy }
    let(:test_taxon) { test_taxonomy.root }
    let(:test_product) { create(:product, taxons: [test_taxon]) }

    before do
      get :show, params: { id: test_taxon.id }
    end

    it "200 OK の成功レスポンスが返って来る" do
      expect(response).to have_http_status "200"
    end

    it "showテンプレートを表示する" do
      expect(response).to render_template :show
    end

    it "@taxonを取得する" do
      expect(assigns(:taxon)).to eq test_taxon
    end

    it "@taxonomiesを取得する" do
      expect(assigns(:taxonomies)).to match_array test_taxonomy
    end

    it "@productsを取得する" do
      expect(assigns(:products)).to match_array test_product
    end
  end
end
