require 'rails_helper'
RSpec.describe Potepan::ProductsController, type: :controller do
  describe "GET #show" do
    let(:product) { create :product }

    before do
      get :show, params: { id: product.id }
    end

    it "200 OK の成功レスポンスが返って来る" do
      expect(response).to have_http_status "200"
    end

    it "showテンプレートを表示する" do
      expect(response).to render_template :show
    end

    it "@productを取得する" do
      expect(assigns(:product)).to eq product
    end
  end
end
